#ifndef __SQL_H__
#define __SQL_H__

struct info
{
	char name[100];
	char phone[100];
	char ring[100];
};

void sql_creat(sqlite3 *db);
void sql_insert(sqlite3 *db,struct info *phone_book);
void sql_del(sqlite3 *db, struct info *phone_book);
void sql_show(sqlite3 *db);

#endif
