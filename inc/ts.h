#ifndef __TS_H__
#define __TS_H__

struct coordinate
{
	int x;
	int y;
};
void ts_trace(int ts, struct coordinate *coor);

#include <stdio.h>
#include <stdbool.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <pthread.h>

#endif
