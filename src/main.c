#include <stdio.h>
#include "common.h"
#include "bmp.h"
#include "sql.h"
#include "ts.h"

char * init_lcd(struct fb_var_screeninfo *vinfo)
{
	int lcd = open(LCD, O_RDWR);

	bzero(vinfo, sizeof(struct fb_var_screeninfo));
	ioctl(lcd, FBIOGET_VSCREENINFO, vinfo);

	int frm_size = vinfo->xres * vinfo->yres * vinfo->bits_per_pixel/8;

	char *FB = mmap(NULL, frm_size, PROT_READ|PROT_WRITE, MAP_SHARED, lcd, 0);

	return FB;
}

int main(void)
{
	//数据库
	sqlite3 *db;
	//电话本
	struct info *phone_book = malloc(sizeof(struct info));
	bzero(phone_book, sizeof(struct info));
	
	struct fb_var_screeninfo vinfo;
	char *FB = init_lcd(&vinfo);
	//保存当前画面
	int frm_size = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel/8;
	char *reserve = calloc(1, frm_size);
	//显示背景
	memcpy(reserve, FB, frm_size);
	bmp2lcd(BACKGROUND, FB, &vinfo, 0, 0);
	bmp2lcd(PLUS, FB, &vinfo, 150, 200);
	bmp2lcd(DELETE, FB, &vinfo, 350, 200);
	bmp2lcd(REVISE, FB, &vinfo, 550, 200);

	int ts = open(TS, O_RDONLY);
	//打开数据库
	if(sqlite3_open(SQLITE, &db) != 0)
	{
		perror("sqlite3_open");
		exit(0);
	}

	sql_creat(db);
	struct coordinate coor;
	
	while(1)
	{
		sql_show(db);
		ts_trace(ts, &coor);
		if(coor.x>150 && coor.x<250 && coor.y>200 && coor.y<300)
		{
			printf("请输入要存储的电话\n");
			scanf("%s", phone_book->phone);
			printf("%s\n", phone_book->phone);

			printf("请输入要存储的姓名\n");
			scanf("%s", phone_book->name);
			printf("%s\n", phone_book->name);
			
			printf("请输入要存储的铃声\n");
			scanf("%s", phone_book->ring);
			printf("%s\n", phone_book->ring);

			sql_insert(db, phone_book);
			continue;
		}
		
		if(coor.x>350 && coor.x<450 && coor.y>200 && coor.y<300)
		{
			printf("请输入要删除的号码\n");
			scanf("%s", phone_book->phone);
			sql_del(db, phone_book);
			continue;
		}
		if(coor.x>550 && coor.x<650 && coor.y>200 && coor.y<300)
		{
			printf("请输入要修改的号码\n");
			scanf("%s", phone_book->phone);
			printf("请输入要修改的内容的名字\n");
			scanf("%s", phone_book->name);
			printf("请输入要修改的内容的铃声\n");
			scanf("%s", phone_book->ring);

			sql_revise(db, phone_book);
			continue;
		}
	
		if(coor.x>700 && coor.y<100)
			break;
	}
	printf("see you!\n");
	sqlite3_close(db);

	memcpy(FB, reserve, frm_size);
	munmap(FB, frm_size);
	close(ts);

	return 0;
}
